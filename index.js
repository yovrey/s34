const express = require("express")
const app = express()
const port = 3000

app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.listen(port, () => console.log(`Server is running at localhost:${port}`))



// 1. Create a GET route that will access the /home route that will print out a simple message.


app.get("/home", (request, response) => {
	response.send("Welcome to the home page.")
})



// 3. Create a GET route that will access the /users route that will retrieve all the users in the mock database.

let users = [
	{
		"username": "johndoe",
		"password": "johndoe1234"
	},
	{
		"username": "janedoe",
		"password": "janedoe1234"
	}
];

app.get("/users",(request, response) => {
	response.send(users)
})


// 5. Create a DELETE route that will access the /delete-user route to remove a user from the mock database.


app.delete("/delete-user", (request, response) => {

	for(let i = 0; i < users.length; i++) {
		
		if(request.body.username == users[i].username){
			users.splice(i, 1)

			message = `User has been deleted!`
			
			break
		} 
		else {
			message = "User does not exist!"
		}
	}

	response.send(message)

})